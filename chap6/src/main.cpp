#include <iostream>
#include "logic.h"

int main()
{
  Complex a,
    b("f(g(Y), f(g(a), Z))"),
    c("f(X11, g(b))"),
    d("f(a, Y1234)"),
    e("g(a)"),
    f(b);

  Substitution s, s1, s2, s3;

  std::cout << "Parsing test :" << std::endl
            << a << std::endl
            << f << std::endl;
  /*
   * Outputs:
   * ()
   * f(g(Y), f(g(a), Z))
   */

  Complex test1("f(X1)"), test2("f(a)");
  std::cout <<  std::endl <<"Simple unification text :" << std::endl
            << test1 << std::endl
            << test2 << std::endl
            << test1.unify(test2,s1) << std::endl
            << s1 << std::endl;

  a="f(X, f(X,Y))";
  std::cout <<  std::endl <<"Recursive unification text :" << std::endl
            << a << std::endl
            << b << std::endl
            << a.unify(b,s) << std::endl;
  /*
   * Outputs:
   * f(X, f(X, Y))
   * f(g(Y), f(g(a), Z))
   * 1
   */

  std::cout << std::endl << "Substitution display test :" << std::endl
            << s << std::endl;

  std::cout << std::endl << "Comparison test :" << std::endl
            << "a==b -> "<< (a==b) << std::endl
            << "a==a -> " <<(a==a) << std::endl;

  c.unify(d, s2);
  s3 = s2;
  std::cout << std::endl << "Comparison test :" << std::endl
            << "s=" << s << std::endl
            << "s2=" << s2 << std::endl
            << "s3=" << s3 << std::endl
            << "s==s ->  " << ( s == s ) << std::endl
            << "s==s2 -> " << ( s == s2 ) << std::endl;

  std::cout<< e << std::endl;

  e=d;
  std::cout << e << " " << d << std::endl;

  return 0;
}